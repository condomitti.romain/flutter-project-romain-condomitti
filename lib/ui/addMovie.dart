import 'package:flutter/material.dart';
import '/model/movie.dart';

class AddMovie extends StatefulWidget {
  @override
  _AddMovieState createState() => _AddMovieState();
}

class _AddMovieState extends State<AddMovie> {
  final _formKey = GlobalKey<FormState>();
      String title = '';
  String year= '';
  String rated= '';
  String released= '';
  String runtime= '';
  String genre= '';
  String director= '';
  String writer= '';
  String actors= '';
  String plot= '';
  String language= '';
  String country= '';
  String awards= '';
  String poster= '';
  String metascore= '';
  String imdbRating= '';
  String imdbVotes= '';
  String imdbID= '';
  String type= '';
  String response= '';
  String image= '';
  List<String> images= [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add a movie"),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(labelText: 'Title'),
                  onSaved: (value) => title = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the title';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Year'),
                  onSaved: (value) => year = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the year';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Rated'),
                  onSaved: (value) => rated = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the rated';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'released'),
                  onSaved: (value) => released = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the released';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'runtime'),
                  onSaved: (value) => runtime = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the runtime';
                    }
                    return null;
                  },
                ),

                TextFormField(
                  decoration: InputDecoration(labelText: 'genre'),
                  onSaved: (value) => genre = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the genre';
                    }
                    return null;
                  },
                ),

                TextFormField(
                  decoration: InputDecoration(labelText: 'director'),
                  onSaved: (value) => director = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the director';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'writer'),
                  onSaved: (value) => writer = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the writer';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'actors'),
                  onSaved: (value) => actors = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the actors';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'plot'),
                  onSaved: (value) => plot = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the plot';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'language'),
                  onSaved: (value) => language = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the language';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'country'),
                  onSaved: (value) => country = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the country';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'awards'),
                  onSaved: (value) => awards = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the awards';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'poster'),
                  onSaved: (value) => poster = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the poster';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'metascore'),
                  onSaved: (value) => metascore = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the metascore';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'imdbRating'),
                  onSaved: (value) => imdbRating = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the imdbRating';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'imdbVotes'),
                  onSaved: (value) => imdbVotes = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the imdbVotes';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'imdbID'),
                  onSaved: (value) => imdbID = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the imdbID';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'type'),
                  onSaved: (value) => type = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the type';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'response'),
                  onSaved: (value) => response = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the response';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'images'),
                  onSaved: (value) => image = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the images';
                    }
                    return null;
                  },
                ),
                // ... add the remaining fields ...
                ElevatedButton(
                  onPressed: (){
                    if (_formKey.currentState!.validate()) {
                      _formKey.currentState!.save();
                      Movie movie = Movie(
                          title: title,
                          year: year,
                          rated: rated,
                          released: released,
                          runtime: runtime,
                          genre: genre,
                          director: director,
                          writer: writer,
                          actors: actors,
                          plot: plot,
                          language: language,
                          country: country,
                          awards: awards,
                          poster: poster,
                          metascore: metascore,
                          imdbRating: imdbRating,
                          imdbVotes: imdbVotes,
                          imdbID: imdbID,
                          type: type,
                          response: response,
                          images: image.split(","));
                      Navigator.pop(context, movie);
                    }
                  },
                  child: Text("Add movie"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }


}