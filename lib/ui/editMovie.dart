import 'package:flutter/material.dart';
import '/model/movie.dart';

class EditMovie extends StatefulWidget {

  final Movie movie;

  EditMovie({Key? key, required this.movie}) : super(key: key);

  @override
  _EditMovieState createState() => _EditMovieState();
}

class _EditMovieState extends State<EditMovie> {
  final _formKey = GlobalKey<FormState>();
  String _title = '';
  String _year= '';
  String _rated= '';
  String _released= '';
  String _runtime= '';
  String _genre= '';
  String _director= '';
  String _writer= '';
  String _actors= '';
  String _plot= '';
  String _language= '';
  String _country= '';
  String _awards= '';
  String _poster= '';
  String _metascore= '';
  String _imdbRating= '';
  String _imdbVotes= '';
  String _imdbID= '';
  String _type= '';
  String _response= '';
  late int index;
  List<String>  _images= [];

  @override
  void initState() {
    super.initState();
    _title = widget.movie.title;
    _year = widget.movie.year;
    _rated = widget.movie.rated;
    _released = widget.movie.released;
    _runtime = widget.movie.runtime;
    _genre = widget.movie.genre;
    _director = widget.movie.director;
    _writer = widget.movie.writer;
    _actors = widget.movie.actors;
    _plot = widget.movie.plot;
    _language = widget.movie.language;
    _country = widget.movie.country;
    _awards = widget.movie.awards;
    _poster = widget.movie.poster;
    _metascore = widget.movie.metascore;
    _imdbRating = widget.movie.imdbRating;
    _imdbVotes = widget.movie.imdbVotes;
    _imdbID = widget.movie.imdbID;
    _type = widget.movie.type;
    _response = widget.movie.response;
    _images = List<String>.from(widget.movie.images);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("edit a movie"),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(labelText: 'Title'),
                  initialValue: _title,
                  onSaved: (value) => _title = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the title';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Year'),
                  initialValue: _year,
                  onSaved: (value) => _year = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the year';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'Rated'),
                  initialValue: _rated,
                  onSaved: (value) => _rated = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the rated';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'released'),
                  initialValue: _released,
                  onSaved: (value) => _released = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the released';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'runtime'),
                  initialValue: _runtime,
                  onSaved: (value) => _runtime = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the runtime';
                    }
                    return null;
                  },
                ),

                TextFormField(
                  decoration: InputDecoration(labelText: 'genre'),
                  initialValue: _genre,
                  onSaved: (value) => _genre = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the genre';
                    }
                    return null;
                  },
                ),

                TextFormField(
                  decoration: InputDecoration(labelText: 'director'),
                  initialValue: _director,
                  onSaved: (value) => _director = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the director';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'writer'),
                  initialValue: _writer,
                  onSaved: (value) => _writer = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the writer';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'actors'),
                  initialValue: _actors,
                  onSaved: (value) => _actors = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the actors';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'plot'),
                  initialValue: _plot,
                  onSaved: (value) => _plot = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the plot';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'language'),
                  initialValue: _language,
                  onSaved: (value) => _language = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the language';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'country'),
                  initialValue: _country,
                  onSaved: (value) => _country = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the country';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'awards'),
                  initialValue: _awards,
                  onSaved: (value) => _awards = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the awards';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'poster'),
                  initialValue: _poster,
                  onSaved: (value) => _poster = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the poster';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'metascore'),
                  initialValue: _metascore,
                  onSaved: (value) => _metascore = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the metascore';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'imdbRating'),
                  initialValue: _imdbRating,
                  onSaved: (value) => _imdbRating = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the imdbRating';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'imdbVotes'),
                  initialValue: _imdbVotes,
                  onSaved: (value) => _imdbVotes = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the imdbVotes';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'imdbID'),
                  initialValue: _imdbID,
                  onSaved: (value) => _imdbID = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the imdbID';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'type'),
                  initialValue: _type,
                  onSaved: (value) => _type = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the type';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: InputDecoration(labelText: 'response'),
                  initialValue: _response,
                  onSaved: (value) => _response = value!,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Please enter the response';
                    }
                    return null;
                  },
                ),
                // ... add the remaining fields ...
                ElevatedButton(
                  onPressed: (){
                    if (_formKey.currentState!.validate()) {
                      _formKey.currentState!.save();
                      Movie movie = Movie(
                          title: _title,
                          year: _year,
                          rated: _rated,
                          released: _released,
                          runtime: _runtime,
                          genre: _genre,
                          director: _director,
                          writer: _writer,
                          actors: _actors,
                          plot: _plot,
                          language: _language,
                          country: _country,
                          awards: _awards,
                          poster: _poster,
                          metascore: _metascore,
                          imdbRating: _imdbRating,
                          imdbVotes: _imdbVotes,
                          imdbID: _imdbID,
                          type: _type,
                          response: _response,
                          images: _images);
                      Navigator.pop(context, movie);
                    }
                  },
                  child: Text("edit movie"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }


}